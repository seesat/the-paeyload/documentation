# IUU Fishing Payload

## Anwesende

Stefan, Dennis, Julian, Jonna, Brendan, Marina, Edgar

## 1. Projekt-Verantwortliche

Nomimierungen: Stefan (bisschen busy mit SeeSat-Baustellen), Marina (weniger erfahren, ...)
Min. 2 Jahre Commitment erwartet
Idee: Stefan als externe Kontaktperson. Marina übernimmt interne Administration

## 2. Kapiteleinteilung / -verantowrtung

- **Zeitplan und Programm:** Marina
- **Orbit:** Edgar
    - Ggf. auf Requirements der (deutschen) Launcher achten. Ansonsten bisher keine Vorgaben der Ausschreibung
    - Nicht SSO da Beobachtung bei Nacht?
    - Uplink über andere Relais? Iridium?
    - Dauer maximales Iridium Blackouts? Wie hoch Iridium Availability im Orbit?
- **Nutzlast:** Julian, Marina, Dennis, (Brendan)
    - Elektrisch, Mechanisch, Thermisch, (Kamera-Definition, AIS Transponder Receiver)
    - Kamera: Marina
    - Iridium: Julian
    - AIS: Dennis
    - Prozessor + Speicher: ???
- **Plattform:** Jonna
    - Marktrecherche
    - idealweise mit Bodensegment
    - Ggf. deutsche Supplier
- **Bodensegment:** ???
- **CAD / Nutzlastbild:** Paul ???

## 3. Technische Details

- Ziel:  Aufdeckung IUU Fishing
- **Nachts: Matching Fischereilicht und KEIN eingeschalteter Transponder**
- ~~Tagsüber: Andockende Boote für Fische Umladen~~

- Wo: Südamerika, Südostasien

### Welches Transponder-Signal

- Stefan: **AIS** statt besseres (?) VMS
    - VMS aufwendiger, mehrere Implementierungen existent
- ~~Ggf. matching im Bodensegment ergänzt durch öffentliche AIS **&** VMS Daten im Netz (aus API)~~

### Real-Time Capability

- Bei Match direkt Uplink an Konstellation (z.B. Iridium)
- Allerdings: Nicht immer Relais über Satellit -> Öffnungswinkel Iridium Satelliten beachten
- Almanach der Konstellation speichern und beim nächsten Unterflug Uplink zum Iridium-Netz

### Pointing

- Bilder über Wasser -> keine Referenz
- D.h. Akkurates Pointing notwendig, GNSS & ADCS
- Existieren andere Referenzen? Boyen?

### Kamera

- Reduktion Komplexität, Weniger andere Mission, Wissenschaftlicher Mehrwert -> **Fokus auf Nacht**
- **Monochrom** / Nicht Monochrom?
- Auflösung? Angestrebt 50x50m GSD

### OBC + TMTC

- Dennis schlägt SeeSat RFSoC vor (Sensor Fusion, viele Frequenzen)
- TRL der Hardware?
- vllt. kommerzielle CubeSat SDR verfügbar

### Bodensegment

- Lediglich Iridium -> Keine eigenes Ops notwendig?
- Real-Time Push zur Iridium bei Match
- Bilder, Messdaten, etc... Downlink über eigenes (?) Bodensegment zeitverzögert -> Zwichenspeicher

## 4. Zeitplanung bis DLR Deadline

- **Montag, 14.08.2023 20:00:** Worksession Nutzlast
- **Dienstag, 15.08.2023:** Päyload Weekly
    - Abstimmung und Auswertung aller Rechercheergebnisse -> Diskussion
    - Einreichung Namensvorschläge
- **Mittwoch, 16.08.2023:** Controller Meeting ???
- **Freitag, 18.09.2023:** Final Review
- **Sonntag, 20.08.2023:** DLR Deadline
