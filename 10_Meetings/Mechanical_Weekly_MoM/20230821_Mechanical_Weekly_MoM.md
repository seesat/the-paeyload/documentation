# 2023-08-21 Meetings Minutes

## Attendees
Paul, Lena, Dominik

## Status The Päyload
- Did not enter a proposal to the Kleinsatellitennutzlastwettbewerb
- Will try to follow up on the AIS and light tracking approach to combat illegal fishing
- Most important issue currently: Stowable small form factor AIS antenna (~0.9[m] of antenna length required for lambda-half)
- Mechanical responsibility: find an appropriate packaging solution for stowing a 0.9[m] antenna on a cubesat

## AIS Antenna

- Coming weeks: Collect ideas for antenna mechanism
- CubeSat form factor: probably around ~6U (maybe larger, probably not smaller)

## Thermal Modelling

- could start 1D thermal model (low priority)

## Organizational

- need CAD and PLM software solution
- should probably set-up document template for design concepts

## Current Antenna Design Ideas



## Questions

1. Does antenna have a preferred orientation?

First guess: Everything in parallel to the Earth's surface is fine.
Maybe ask antenna people.

2. How can the antenna be split up into parts, i.e. how can the connection between segments be designed to achieve good performance?

Ask telecommunications.

3. Does the antenna have to be inside the cubesat form factor at launch?

Yes, except for a pontential "tuna can" (i.e. U+ configuration).

4. Can 6U of CubeSat modules be configured in a 1x6 form factor?

Probably not, as dispensers are standardized between launch providers. Might be worth asking though.
