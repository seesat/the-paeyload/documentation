# 2023-10-09 Meetings Minutes

## Attendees
Paul, Lena, Martin

## Status NEMO&DORY

- AIS and light tracking approach to combat illegal fishing
- Stowable small form factor AIS antenna (~0.9[m] of antenna length required for lambda-half)
- Mechanical responsibility: find an appropriate packaging solution for stowing a 0.9[m] antenna on a cubesat

## AIS Antenna

- Coming weeks: Collect ideas for antenna mechanism
- CubeSat form factor: probably around ~6U (maybe larger, probably not smaller)

## Thermal Modelling

- could start 1D thermal model (low priority)

## Organizational

- need CAD and PLM software solution
- should probably set-up document template for design concepts
- October 15th: photo shooting, could have a worksession on same day

- tendency toward either in-person meetings on Sunday afternoon, or weekday evening meetings
- lead role can stay with Paul until everyone is a little more experienced, then might switch responsibility

## Constraints



## Current Antenna Design Ideas

### Concepts

1. Folding segments
2. Concentric Rods


## Questions

1. Does antenna have a preferred orientation?

First guess: Everything in parallel to the Earth's surface is fine.
Antenna people: Have to look into it.

2. How can the antenna be split up into parts, i.e. how can the connection between segments be designed to achieve good performance?

Antenna people: Have to look into it.

3. Does the antenna have to be inside the cubesat form factor at launch?

Yes, except for a pontential "tuna can" (i.e. U+ configuration).

4. Can 6U of CubeSat modules be configured in a 1x6 form factor?

Probably not, as dispensers are standardized between launch providers. Might be worth asking though.

5. What resources can be used for initial research?

Google Scholar, oa.mg, books (for detailed analysis), ECSS standards
