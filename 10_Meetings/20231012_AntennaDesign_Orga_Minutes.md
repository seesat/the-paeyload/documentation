# 2023-10-12 Antenna Design Organization Meeting

Goals:
Organizing antenna design for coming semester

Projects to be discussed:
- DORY
- S-band antenna SeeSat-1

## SeeSat-1:

- Marvin stays with S-band antenna
- Hannah could advise Studienarbeit for SeeSat-1 S-band antenna
- questions can be put into TMTC channel

## DORY:

- Paul can take responsibility for project overview and general advice
- Dennis has to advise RF antenna design
- would require very close advice for antenna design due to large design space
- many options to be analysed

- discuss NEMO mission design interplay with DORY antenna design on Sat, Oct 14th

## Ways of communication

Channels:
- DORY working group
- TMTC advisors
- TMTC working group with students
