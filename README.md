# Documentation

## Link to Wiki

https://gitlab.com/seesat/the-paeyload/documentation/-/wikis/home

## Git basics

### Clone the Repository to your computer

- Install Git (https://git-scm.com) on your local computer
- Create a directory where you want to have the data
- Open a command line in this directory

```
git clone https://gitlab.com/seesat/the-paeyload/documentation.git
```

### Add a new local file

```
git add my_file.txt
```

### Publish changes to everybody else

```
git commit
git push
```

### Branches

- [All about branches](https://docs.gitlab.com/ee/user/project/repository/branches/)
- [How to create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
