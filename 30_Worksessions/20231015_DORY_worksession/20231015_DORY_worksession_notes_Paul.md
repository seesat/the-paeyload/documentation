# Notes on DORY worksession 2023-10-15

Goals:
- fully modular antenna concept
- simple design to reduce cost and make assembly easier

Necessary Parts for radio assembly:

- attachment concept
- antenna boom concept
- launch stowing concept
- launch retention mechanism
- release mechanism
- retention mechanism (launch+operational)

Requirements:

- available assembly volume
- maximum AOCS torque
- full separation possible (modularity)
- allowable number of boom segments (antenna efficiency)
- allowable antenna orientation
- maximum mass
- maximum cost

Ways of judging concept complexity:

- number of parts
- number of unique parts
- milling required?
- turning required?
- 3/4/5-axis mill required?
- granular tolerances?
