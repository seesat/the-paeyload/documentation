# 2023-10-15 DORY work session agenda

1. Short introduction				30min
	- context
	- mission goals
2. Break					15min
3. High-Level Goal discussion			60min
	- use of previous discussion results
	- setting goals and constraints overall
4. Break					15min
5. Morphological Discussion 1			60min
	- determining necessary parts for radio assembly
	- defining initial ideas and concepts for different parts
6. Break					15min
7. Morphological Discussion 2			60min
	- setting rough advantages, disadvantages and points to be cleared
	- connecting part ideas to some overall concepts
8. Break					15min
9. Analysis Discussion and Task Distribution	60min
	- defining first analysis goals
	- setting tasks to be fulfilled next
	- distributing tasks 

Overall Time:					5h30min
